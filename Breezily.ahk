;SETTINGS
#InstallKeybdHook
#MaxThreadsPerHotkey, 2
#MaxThreadsBuffer, On
SetTitleMatchMode 2
SendMode Input

;semicolon indicates a comment

;VARIABLES
alt_key=!
ctrl_key=^
shift_key=+
windows_key=#
0_key=0
1_key=1
2_key=2
3_key=3
apps_key=AppsKey
bullet_symbol=U+2022
backspace_key=backspace
delete_key=delete
down_key=down
end_key=end
enter_key=enter
escape_key=escape
f2_key=f2
f4_key=f4
home_key=home
left_key=left
left_mouse_btn=LButton
minus_key=NumpadSub
pageup_key=PgUp
plus_key=NumpadAdd
pagedown_key=PgDn
right_key=right
right_mouse_btn=RButton
up_key=up
tab_key=tab
win_key_down=LWinDown
win_key_up=LWinUp

;VIRUAL KEYS
A_key=vk41
B_key=vk42
C_key=vk43
D_key=vk44
F_key=vk46
I_key=vk49
J_key=vk4A
L_key=vk4C
N_key=vk4E
O_key=vk4F
P_key=vk50
R_key=vk52
S_key=vk53
T_key=vk54
U_key=vk55
V_key=vk56
W_key=vk57
X_key=vk58
Y_key=vk59
Z_key=vk5A
backtick_key=vkC0

;ESSENTIAL SHORTCUTS
F23 & Space::KeypressDouble_E2(win_key_down, win_key_up) ;press windows key
F23 & Q::Keypress(escape_key) ;press escape key
F23 & Z::KeypressDouble_E1(ctrl_key, Z_key) ;undo previous command
F23 & X::KeypressTriple_E1(ctrl_key, shift_key, Z_key) ;reverse previous undo command
F23 & C::KeypressDouble_E1(ctrl_key, C_key) ;copy selected item to clipboard
F23 & I::KeypressDouble_E1(ctrl_key, X_key) ;cut selected item to clipboard
F23 & V::KeypressDouble_E1(ctrl_key, V_key) ;paste contents of clipboard into current document/app
F23 & G::KeypressDouble_E1(ctrl_key, F_key) ;find items in document or open find tool
F23 & S::KeypressDouble_E1(ctrl_key, S_key) ;save current document/item
LShift & Space::Keypress(f2_key) ;rename current selection
RShift & Space::KeypressTriple_E1(ctrl_key, shift_key, N_key) ;create a new directory
F23 & 9::Keypress(bullet_symbol) ;insert bullet unicode character
F23 & Enter::KeypressDouble_E1(ctrl_key, enter_key) ;press control key and enter key simultaneously
RAlt & Q::KeypressDouble_E1(alt_key, f4_key) ;quit an application

;DOCUMENT SHORTCUTS
F23 & D::Keypress(delete_key) ;delete character to right of insertion point
F23 & H::Keypress(backspace_key) ;delete character to left of insertion point
F23 & B::Keypress(left_key) ;move one character backward
F23 & F::Keypress(right_key) ;move one character forward
F23 & P::Keypress(up_key) ;move up one line
F23 & N::Keypress(down_key) ;move down one line
F23 & O::Keypress(pageup_key) ;scroll up on current page
F23 & L::Keypress(pagedown_key) ;scroll down on current page
F23 & Y::KeypressDouble_E1(ctrl_key, A_key) ;highlight/select all items
F23 & A::Keypress(home_key) ;move to beginning of line/paragraph
F23 & E::Keypress(end_key) ;move to end of line/paragraph.
F23 & K::KeypressTriple_E2(shift_key, end_key, delete_key) ;delete text between insertion point and end of line/paragraph.
F23 & `;::KeypressDouble_E1(shift_key, left_key) ;extend text selection one character to left
F23 & '::KeypressDouble_E1(shift_key, right_key) ;extend text selection one character to right
F23 & {::KeypressTriple_E1(ctrl_key, shift_key, left_key) ;extend text selection to beginning of current word, then to beginning of  following word if pressed again
F23 & }::KeypressTriple_E1(ctrl_key, shift_key, right_key) ;extend text selection to end of current word, then to end of following word if pressed again
F23 & BackSpace::KeypressDouble_E1(shift_key, home_key) ;select text between insertion point and beginning of current line
F23 & \::KeypressDouble_E1(shift_key, end_key) ;select text between insertion point and end of current line
F23 & U::KeypressDouble_E1(shift_key, up_key) ;extend text selection to nearest character at same horizontal location on line above
F23 & J::KeypressDouble_E1(shift_key, down_key) ;extend text selection to nearest character at same horizontal location on line below

;DESKTOP SHORTCUTS
F23 & RCtrl::KeypressDouble_E1(windows_key, tab_key) ;open task view panel
F23 & 8::KeypressTriple_E1(windows_key, ctrl_key, D_key) ;add a new virtual desktop
F23 & 5::KeypressTriple_E1(windows_key, ctrl_key, right_key) ;switch between virtual desktops you’ve created on the right
F23 & 7::KeypressTriple_E1(windows_key, ctrl_key, left_key) ;switch between virtual desktops you’ve created on the left
F23 & 6::KeypressTriple_E1(windows_key, ctrl_key, f4_key) ;close active virtual desktop

;WINDOW SHORTCUTS
F23 & 4::KeypressDouble_E1(ctrl_key, N_key) ;open a new window
F23 & W::KeypressDouble_E1(ctrl_key, W_key) ;close active window
F23 & M::KeypressDouble_E1(windows_key, down_key) ;minimize current window to task bar
F23 & <::KeypressDouble_E1(windows_key, left_key) ;maximize current window to left side of screen
F23 & >::KeypressDouble_E1(windows_key, right_key) ;maximize current window to right side of screen
F23 & /::KeypressDouble_E1(windows_key, up_key) ;maximize current window

;WEB BROWSER SHORTCUTS
F23 & R::KeypressDouble_E1(ctrl_key, R_key) ;reload current page.
F23 & T::KeypressDouble_E1(ctrl_key, T_key) ;open a new tab, and move to it
F23 & Tab::KeypressDouble_E1(ctrl_key, tab_key) ;move to next open tab
F23 & LAlt::KeypressTriple_E1(ctrl_key, shift_key, tab_key) ;move to previous open tab
F23 & RShift::KeypressTriple_E1(ctrl_key, shift_key, tab_key) ;move to previous open tab
F23 & `::KeypressTriple_E1(ctrl_key, shift_key, T_key) ;reopen last closed tab and move to it
F23 & 1::KeypressDouble_E1(alt_key, left_key) ;open previous page from browsing history in  current tab
F23 & 2::KeypressDouble_E1(alt_key, right_key) ;open next page from browsing history in  current tab
F23 & 3::KeypressDouble_E1(ctrl_key, L_key) ;move to address bar
F23 & 0::KeypressDouble_E1(ctrl_key, 0_key) ;return contents on page to default size.
F23 & -::KeypressDouble_E1(ctrl_key, minus_key) ;decrease size of current page.
F23 & +::KeypressDouble_E1(ctrl_key, plus_key) ;increase size of current page.

;MOUSE CURSOR SHORTCUTS
!C::Keypress(left_mouse_btn)
!V::MouseClickRight()
!W::MouseMovePointerUp()
!A::MouseMovePointerLeft()
!S::MouseMovePointerDown()
!D::MouseMovePointerRight()
+!W::MouseMovePointerUpDoubled()
+!A::MouseMovePointerLeftDoubled()
+!S::MouseMovePointerDownDoubled()
+!D::MouseMovePointerRightDoubled()
^!W::MouseMovePointerUpHalved()
^!A::MouseMovePointerLeftHalved()
^!S::MouseMovePointerDownHalved()
^!D::MouseMovePointerRightHalved()
!1::MouseMovePointerTopLeft()
!2::MouseMovePointerTopCenter()
!3::MouseMovePointerTopRight()
!4::MouseMovePointerMiddleLeft()
!5::MouseMovePointerMiddleCenter()
!6::MouseMovePointerMiddleRight()
!7::MouseMovePointerBottomLeft()
!8::MouseMovePointerBottomCenter()
!9::MouseMovePointerBottomRight()
!0::MouseMovePointerWindowCenter()

;KEYPRESS FUNCTIONS
Keypress(key) {
 SendInput, {%key%}
}
; E1: enclose 1 key in braces {}
KeypressDouble_E1(key_1, key_2) {
  SendInput, %key_1%{%key_2%}
}

KeypressTriple_E1(key_1, key_2, key_3) {
  SendInput, %key_1%%key_2%{%key_3%}
}
; E2: enclose 2 keys in braces {}
KeypressDouble_E2(key_1, key_2) {
  SendInput, {%key_1%}{%key_2%}
}

KeypressTriple_E2(key_1, key_2, key_3) {
  SendInput, %key_1%{%key_2%}{%key_3%}
}

;MOUSE POINTER FUNCTIONS
GetScreenHeight() {
  return A_ScreenHeight
}

GetScreenWidth() {
  return A_ScreenWidth
}

GetMouseMoveDistanceFactor() {
  return 0.0275
}

GetMouseMoveDistanceFactorDoubled() {
  return GetMouseMoveDistanceFactor() * 2
}

GetMouseMoveDistanceFactorHalved() {
  return GetMouseMoveDistanceFactor() / 2
}

GetPointerOffsetDistance(size, factor) {
  offset_distance := size * factor
  return offset_distance
}

MultiplyBy(number, multiplier) {
  return (number * (multiplier))
}

MouseClickRight() {
  MouseGetPos, x_pos, y_pos
  Click %x_pos%, %y_pos% Right
  return
}

MouseMovePointerUp() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactor()
  screen_height := GetScreenHeight()
  y_position := y_pos - GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerUpDoubled() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorDoubled()
  screen_height := GetScreenHeight()
  y_position := y_pos - GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerUpHalved() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorHalved()
  screen_height := GetScreenHeight()
  y_position := y_pos - GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerLeft() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactor()
  screen_width := GetScreenWidth()
  x_position := x_pos - GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerLeftDoubled() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorDoubled()
  screen_width := GetScreenWidth()
  x_position := x_pos - GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerLeftHalved() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorHalved()
  screen_width := GetScreenWidth()
  x_position := x_pos - GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerDown() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactor()
  screen_height := GetScreenHeight()
  y_position := y_pos + GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerDownDoubled() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorDoubled()
  screen_height := GetScreenHeight()
  y_position := y_pos + GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerDownHalved() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorHalved()
  screen_height := GetScreenHeight()
  y_position := y_pos + GetPointerOffsetDistance(screen_height, distance_factor)
  MouseMove, %x_pos%, %y_position%
  return
}

MouseMovePointerRight() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactor()
  screen_width := GetScreenWidth()
  x_position := x_pos + GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerRightDoubled() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorDoubled()
  screen_width := GetScreenWidth()
  x_position := x_pos + GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerRightHalved() {
  MouseGetPos, x_pos, y_pos
  distance_factor := GetMouseMoveDistanceFactorHalved()
  screen_width := GetScreenWidth()
  x_position := x_pos + GetPointerOffsetDistance(screen_width, distance_factor)
  MouseMove, %x_position%, %y_pos%
  return
}

MouseMovePointerTopLeft() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/6)
  y_position := MultiplyBy(screen_height, 1/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerTopCenter() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/2)
  y_position := MultiplyBy(screen_height, 1/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerTopRight() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 5/6)
  y_position := MultiplyBy(screen_height, 1/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerMiddleLeft() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/6)
  y_position := MultiplyBy(screen_height, 1/2)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerMiddleCenter() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/2)
  y_position := MultiplyBy(screen_height, 1/2)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerMiddleRight() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 5/6)
  y_position := MultiplyBy(screen_height, 1/2)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerBottomLeft() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/6)
  y_position := MultiplyBy(screen_height, 3/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerBottomCenter() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 1/2)
  y_position := MultiplyBy(screen_height, 3/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerBottomRight() {
  CoordMode, Mouse, Screen
  screen_width := GetScreenWidth()
  screen_height := GetScreenHeight()
  x_position := MultiplyBy(screen_width, 5/6)
  y_position := MultiplyBy(screen_height, 3/4)
  MouseMove, %x_position%, %y_position%
  return
}

MouseMovePointerWindowCenter() {
  WinGetPos, X, Y, window_width, window_height, A
  new_window_width := MultiplyBy(window_width, 1/2)
  new_window_height := MultiplyBy(window_height, 1/2)
  MouseMove, %new_window_width%, %new_window_height%
  Return
}
